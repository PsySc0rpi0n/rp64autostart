#!/bin/bash

CLRESTPATH=$HOME'/Downloads/c-lightning-REST'
RTLPATH=$HOME'/Downloads/RTL-v0.12.1-beta'
BTCPATH=$HOME'/Downloads/bitcoin-0.21.1/bin'
CLPATH=$HOME'/Downloads/c-lightning-v0.10.2/lightningd'

CLRESTPROC='cl-rest.js'
RTLPROC='rtl.js'
CLPROC='lightningd'
IRSSIPROC='irssi'
MCABBERPROC='mcabber'
BITCOINPROC='bitcoind'

PIDCLREST=$(pgrep -f "^node $CLRESTPROC$")
PIDRTL=$(pgrep -f "^node $RTLPATH/$RTLPROC$")

PIDBTC=$(pgrep -f "^$BTCPATH/$BITCOINPROC$")
PIDCL=$(pgrep -f "^$CLPROC$")

#CLRESTDIR="/home/psysc0rpi0n/Downloads/c-lightning-REST/"

##########################################################
############ Check if Bitcoin core is running ############
##########################################################
if [[ -z "$PIDBTC" ]]; then
	echo "Bitcoin Core is not running. Starting it."
	systemctl start bitcoind
fi
echo "Bitcoin is running!"

##########################################################
########## Check if local blockhain is sync'ed ###########
##########################################################
LOCALBLKH=$(bitcoin-cli getblockcount)
CURRENTBLKH=$(curl -s https://api.blockchair.com/bitcoin/stats | jq -r '.data | .blocks')
while [[ LOCALBLKH -lt $((CURRENTBLKH-1)) ]];
   do
      echo "Bitcoin Core is behind the Blockchain height! Cathing up!"	
      echo "Trying again in 1 minute!"
      sleep 60
      LOCALBLKH=$(bitcoin-cli getblockcount)
      CURRENTBLKH=$(curl -s https://api.blockchair.com/bitcoin/stats | jq -r '.data | .blocks')
   done
echo "Bitcoin Core is synch'ed!"

##########################################################
############# Check if c-lightning is running ############
##########################################################
PIDCL=$(pgrep -f "^$CLPROC$")
if [[ -z $PIDCL ]]; then
   screen -S cl-debug -fa -d -m tail -f $HOME/.lightning/debug.log
   sleep 3
	screen -S lightningd -fa -d -m $CLPROC
   echo "c-lightning daemon started!"
else
	echo "$CLPROC node is already running"
fi

##########################################################
########## Check if c-lightning-REST is running ##########
##########################################################
if [[ -z "$PIDCLREST" ]]; then
   curdir=$(pwd)
   if [[ ! "$currdir" -ef "$CLRESTDIR" ]]; then
      cd "$CLRESTPATH"
   fi
   screen -S cl-rest -fa -d -m node $CLRESTPROC
   echo "c-lightning-REST started!"
else
	echo "$CLRESTPROC is already running!"
fi

##########################################################
################ Check if RTL is running #################
##########################################################
if [[ -z "$PIDRTL" ]]; then
   curdir=$(pwd)
   if [[ ! "$curdir" -ef "$RTLPATH" ]]; then
      cd "$RTLPATH"
   fi
   screen -S rtl -fa -d -m node $RTLPATH/$RTLPROC
   echo "RTL started!"
else
   echo $RTLPROC " is already running!"
fi

##########################################################
############### Check if irssi is running ################
##########################################################
PIDIRSSI=$(pgrep -f "^$IRSSIPROC")
if [[ -z $PIDIRSSI ]]; then
   screen -S irssi -fa -d -m irssi
   echo "irssi started!"
else
   echo "$IRSSIPROC is already running!"
fi

##########################################################
############## Check if mcabber is running ###############
##########################################################
PIDMCABBER=$(pgrep -f "^$MCABBERPROC")
if [[ -z $PIDMCABBER ]]; then
   screen -S mcabber -fa -d -m mcabber
   echo "mcabber started!"
else
   echo "$MCABBERPROC is already running!"
fi
